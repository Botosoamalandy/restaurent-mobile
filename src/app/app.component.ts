import { Storage } from '@ionic/storage';
import { AuthentificationService } from './service/authentification.service';
import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  public appPages = [
    {
      title: 'Plats',
      url: 'plats',
      icon: 'fast-food'
    },
    {
      title: 'Nouvelle commande',
      url: 'nouvel-commande',
      icon: 'restaurant'
    },
    {
      title: 'Liste des commandes',
      url: 'liste-commande',
      icon: 'list-circle'
    },
    {
      title: 'Plats insuffisants',
      url: 'insuffisant',
      icon: 'close-circle'
    },
    {
      title: 'Preparation',
      url: 'preparation',
      icon: 'hourglass'
    },
    {
      title: 'Mon compte',
      url: 'moncompte',
      icon: 'person'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private storage:Storage,
    private authentificationService:AuthentificationService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
    const path = window.location.pathname.split('folder/')[1];
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }
  }
  deconnexionCompte(){
    this.authentificationService.deconnexion();
    this.storage.clear();
  }
}
