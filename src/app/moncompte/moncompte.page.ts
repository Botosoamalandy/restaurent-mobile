import { MotDePasseDynamiqueService } from './../service/mot-de-passe-dynamique.service';
import { DiversService } from './../service/divers.service';
import { RequetteService } from './../service/requette.service';
import { AuthentificationService } from './../service/authentification.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-moncompte',
  templateUrl: './moncompte.page.html',
  styleUrls: ['./moncompte.page.scss'],
})
export class MoncomptePage implements OnInit {
  showModificationTemporaire:boolean=false;check:boolean=false;textCheck:string='';textMotDePasse:string='';nom:string;motDePasseActuel:string;
  validationMotDePasse:boolean=false;textConfirmation:string='';motDePasse:string='';validationFinale:boolean=false;confirmation:string='';
  textButtonModification:string='Modification';textIconModification:string='caret-down';
  informationUtilisateur:any={
    nom: '',
    etat: 0,
    date: '',
    password : '',
    motDePasse : ''
  };

  constructor(
    private diversService:DiversService,
    private requetteService:RequetteService,
    private authentificationService:AuthentificationService,
    private motDePasseDynamiqueService:MotDePasseDynamiqueService
  ) { }

  getInformationUtilisateur(){
    this.authentificationService.getToken().then((responsetoken:string)=>{   
      this.requetteService.getRequetteGet('informationUtilisateur?numeroToken='+responsetoken).subscribe((response:any)=>{
        this.informationUtilisateur=response;
        this.nom=this.informationUtilisateur.nom;
      },erreur=>{
        this.diversService.getAlertControllerAffichage('Message d\'erreur','Il y a une erreur lors de la chargement du commande','OK');
        console.log('erreur :',erreur);
      });
    }).catch(erreur=>{
      this.diversService.getAlertControllerAffichage('Message d\'erreur','Il y a une erreur lors de la chargement du commande','OK');
        console.log('erreur :',erreur);
    });
  }
  initialisationFonction(){
    this.authentificationService.getVerificationSessionTokenSiExiste();
    this.getInformationUtilisateur();
  }
  ngOnInit() {
    this.initialisationFonction();
  }
  showModification(){
    if(this.showModificationTemporaire){
      this.showModificationTemporaire=false;
      this.textButtonModification='Modification';
      this.textIconModification='caret-down';
    }else{
      this.showModificationTemporaire=true;
      this.textButtonModification='Annuler';
      this.textIconModification='caret-up';
    }
  }
  //Mot de passe dynamique

  setToDynamiqueMotDepasse(ev: any){
    var text=ev.target.value;
    if(this.motDePasseDynamiqueService.getVerificationLenghtWords(text) && 
    this.motDePasseDynamiqueService.getVerificationIfNumberExist(text) && 
    this.motDePasseDynamiqueService.getVerificationIfAlphabSmallWordExist(text) &&
    this.motDePasseDynamiqueService.getVerificationIfAlphabExist(text) &&
    this.motDePasseDynamiqueService.getVerificationIfWordsSpecialExist(text)){
      document.getElementById('textMDP').style.color='green';
      this.textMotDePasse='Mot de passe complet';
      this.validationMotDePasse=true;this.motDePasse=text;this.disabledBoutonOrNon(text);
    }else{
      document.getElementById('textMDP').style.color='red';
      this.validationMotDePasse=false;this.motDePasse='';
      this.textMotDePasse=
        this.motDePasseDynamiqueService.getAssembleLesLettre(this.motDePasseDynamiqueService.getVerificationLenghtWords(text),'8 caractére minimum')+' '+
        this.motDePasseDynamiqueService.getAssembleLesLettre(this.motDePasseDynamiqueService.getVerificationIfNumberExist(text),'un chiffre')+' '+
        this.motDePasseDynamiqueService.getAssembleLesLettre(this.motDePasseDynamiqueService.getVerificationIfAlphabSmallWordExist(text),'une lettre')+' '+
        this.motDePasseDynamiqueService.getAssembleLesLettre(this.motDePasseDynamiqueService.getVerificationIfAlphabExist(text),'un lettre majuscule')+' '+
        this.motDePasseDynamiqueService.getAssembleLesLettre(this.motDePasseDynamiqueService.getVerificationIfWordsSpecialExist(text),'une caractére speciaux');
        this.disabledBoutonOrNon(text);
    }
  }
  confirmationMotDePasse(ev: any){
    var text=ev.target.value;
    if(this.validationMotDePasse==true && this.motDePasse!='' && this.motDePasse==text){
      this.validationFinale=true;
      this.textConfirmation='Mot de passe confirmé';
      document.getElementById('textConfirmation').style.color='green';
      this.disabledBoutonOrNon(text);this.confirmation=text;
    }else{
      this.validationFinale=false;
      this.textConfirmation='Mot de passe non confirmé';
      document.getElementById('textConfirmation').style.color='red';
      this.disabledBoutonOrNon(text);this.confirmation=text;
    }
  }
  disabledBoutonOrNon(text:string){
    if(this.validationMotDePasse==true && this.motDePasse!='' && this.motDePasse==text){
      (<HTMLInputElement>document.getElementById('bouton')).disabled=false;
    }else{
      (<HTMLInputElement>document.getElementById('bouton')).disabled=true;
    }
  }
  oncheck(){
    console.log(this.check);
    if(this.check==false){
      (<HTMLInputElement>document.getElementById('bouton')).disabled=false;
      this.textCheck='Modifier uniquement le nom d\'utilisateur';
    }else{
      this.textCheck='';
      this.disabledBoutonOrNon(this.confirmation);
    }
  }
  updateUtilisateur(data){
    this.requetteService.getRequettePost('updateUtilisateur',data).subscribe((response:any)=>{
      this.diversService.getAlertControllerAffichage('Message',''+response.message,'Valider');
      this.initialisationFonction();
    },erreur=>{
      this.diversService.getAlertControllerAffichage('Message d\'erreur','Il y a une erreur lors de la chargement du commande','OK');
      console.log('erreur :',erreur);
    });
  }
  modificationUtilisateur(){
    this.authentificationService.getToken().then((responsetoken:string)=>{  
      if(this.check==true && this.motDePasseActuel!=null && this.motDePasseActuel!='' && this.nom!=null && this.nom!='' ){
        console.log('nom :'+this.nom);
        let data ={ 
          nom : this.nom ,
          motDePasse : this.informationUtilisateur.password ,
          motDePasseActuel : this.motDePasseActuel,
          numeroToken : responsetoken 
        }
        this.updateUtilisateur(data);
      }else if(this.nom!=null && this.nom!='' && this.motDePasseActuel!=null && this.motDePasseActuel!='' && this.motDePasse!='' && this.motDePasse!=null && this.validationFinale==true){
        let data ={ 
            nom : this.nom ,
            motDePasse : this.motDePasse ,
            motDePasseActuel : this.motDePasseActuel ,
            numeroToken : responsetoken 
        }
        this.updateUtilisateur(data);
      }else if(this.validationFinale==false && this.check==false){
        alert('Confirmez votre mot de passe');
      }else{	
        alert('Il y a une champs vide ');
      }
    });
  }




}
