import { AlertController } from '@ionic/angular';
import { StorageService } from './../service/storage.service';
import { AuthentificationService } from './../service/authentification.service';
import { RequetteService } from './../service/requette.service';
import { DiversService } from './../service/divers.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  utilisateur:string;passwords:string;sessionToken:boolean=true;
  constructor(
    private router:Router,
    private diversService:DiversService,
    private storageService:StorageService,
    private alertController:AlertController,
    private requetteService:RequetteService,
    private authentificationService:AuthentificationService
  ) { }

  ngOnInit() {
    if(this.sessionToken==true && this.authentificationService.getToken()!=null){
      this.getVerificationIfSessionTokenExiste();
    }
  }

  login(){
    if(this.utilisateur!='' && this.utilisateur!=null && this.passwords!='' && this.passwords!=null){
      var data={nom : this.utilisateur,motDePasse : this.passwords};
      //requtte poste loginMobile in serveur
      this.requetteService.getRequettePost('loginMobile',data).subscribe((successResponse:any)=>{
        let token=''+successResponse.numeroToken;
        if(token!=null && token!=''){
          this.authentificationService.addToken(token);
          this.router.navigateByUrl('/moncompte');
          this.getProduitMobile();
        }else{
          this.diversService.getAlertControllerAffichage('Message','Verifiez votre champs car il y a une erreur','OK');
        }
      },(errorResponse)=>{
        this.diversService.getAlertControllerAffichage('Token','Il y a une erreur','OK');
        console.log('error :',errorResponse);
      });
    }else{
      this.diversService.getAlertControllerAffichage('Message','Tous les champs sont vides','OK');
    }
  }
  getVerificationIfSessionTokenExiste(){
    this.authentificationService.getToken().then((response:string)=>{
      let token=response;
      this.requetteService.getRequetteGet('loginToken?token='+token).subscribe((successResponse:any)=>{
        let resultat=''+successResponse.validation;
        if(resultat=='true'){
          this.getProduitMobile();
          this.router.navigateByUrl('/moncompte');
        }else{
          this.authentificationService.redirectionLogin();
          this.sessionToken=false;
        }
      },(errorResponse)=>{
        this.diversService.getAlertControllerAffichage('Token','Il y a une erreur','OK');
      });
    });
    
  }
  getProduitMobile(){
    this.requetteService.getRequetteGet('produitMobile').subscribe((successResponse:any[])=>{
      this.storageService.addProduit(successResponse);
      console.log('prduit :',successResponse);
    },(errorResponse)=>{
      this.diversService.getAlertControllerAffichage('Message','Il y a une erreur','OK');
      console.log('erreur :',errorResponse);
    });
  }
  motDePasseOublier(){
    this.router.navigateByUrl('mot-de-passe-oublier');
  }
}
