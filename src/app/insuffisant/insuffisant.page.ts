import { Router, NavigationExtras } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { DiversService } from './../service/divers.service';
import { RequetteService } from './../service/requette.service';
import { AuthentificationService } from './../service/authentification.service';
import { Component, OnInit } from '@angular/core';
import { interval} from 'rxjs';

@Component({
  selector: 'app-insuffisant',
  templateUrl: './insuffisant.page.html',
  styleUrls: ['./insuffisant.page.scss'],
})
export class InsuffisantPage implements OnInit {
  insuffisants:any[]=[];
  constructor(
    private router:Router,
    private diversService:DiversService,
    private modalController:ModalController,
    private requetteService:RequetteService,
    private authentificationService:AuthentificationService
  ) { }
  ngOnInit() {
    this.initialisationDonnee();
    interval(5000).subscribe(x=>{
      this.getInsuffisant();
    },error=>{
      console.log('Erreur')
    });
  }
  initialisationDonnee(){
    this.authentificationService.getVerificationSessionTokenSiExiste();
    this.getInsuffisant();
  }
  getInsuffisant(){
    this.authentificationService.getToken().then((response)=>{
      this.requetteService.getRequetteGet('listeInsuffisantByNumeroToken?numeroToken='+response).subscribe((responseRequette:any[])=>{
        this.insuffisants=responseRequette;
        console.log(responseRequette);
      },erreur=>{
        this.diversService.getAlertControllerAffichage('Message d\'erreur','Il y a une erreur','OK');
        console.log(erreur);
      });
    });
  }
  supprimerInsuffisant(insuffisantTemporaire:any){
    this.requetteService.getRequetteGet('supprimerInsuffisant?idCommande='+insuffisantTemporaire.idCommande+'&idInsuffisant='+insuffisantTemporaire.idInsuffisant).subscribe((responseRequette:any)=>{
      this.diversService.getAlertControllerAffichage('Message',''+responseRequette.message,'OK');
      this.getInsuffisant();
    },erreur=>{
      this.diversService.getAlertControllerAffichage('Message d\'erreur','Il y a une erreur','OK');
      console.log(erreur);
    });
  }
  modification(insuffisantTemporaire){
      if(insuffisantTemporaire!=null){
        let navigationExtrs:NavigationExtras={
          state:{
            insuffisant:insuffisantTemporaire
          }
        }
        this.router.navigate(['modification-plats-insuffisant'],navigationExtrs);
        this.getInsuffisant();
      }else{
        this.diversService.getAlertControllerAffichage('Message d\'erreur','Il y a une erreur lors de la chargement du commande','OK');
    }
  }
}
