import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InsuffisantPageRoutingModule } from './insuffisant-routing.module';

import { InsuffisantPage } from './insuffisant.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InsuffisantPageRoutingModule
  ],
  declarations: [InsuffisantPage]
})
export class InsuffisantPageModule {}
