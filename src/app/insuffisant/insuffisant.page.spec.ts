import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InsuffisantPage } from './insuffisant.page';

describe('InsuffisantPage', () => {
  let component: InsuffisantPage;
  let fixture: ComponentFixture<InsuffisantPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsuffisantPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InsuffisantPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
