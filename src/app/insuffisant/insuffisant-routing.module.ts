import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InsuffisantPage } from './insuffisant.page';

const routes: Routes = [
  {
    path: '',
    component: InsuffisantPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InsuffisantPageRoutingModule {}
