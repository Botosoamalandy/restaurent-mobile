import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '',redirectTo: 'login',pathMatch: 'full' },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'plats',
    loadChildren: () => import('./plats/plats.module').then( m => m.PlatsPageModule)
  },
  {
    path: 'nouvel-commande',
    loadChildren: () => import('./nouvel-commande/nouvel-commande.module').then( m => m.NouvelCommandePageModule)
  },
  {
    path: 'liste-commande',
    loadChildren: () => import('./liste-commande/liste-commande.module').then( m => m.ListeCommandePageModule)
  },
  {
    path: 'liste-plats-commander',
    loadChildren: () => import('./liste-plats-commander/liste-plats-commander.module').then( m => m.ListePlatsCommanderPageModule)
  },
  {
    path: 'detail-commande',
    loadChildren: () => import('./detail-commande/detail-commande.module').then( m => m.DetailCommandePageModule)
  },
  {
    path: 'insuffisant',
    loadChildren: () => import('./insuffisant/insuffisant.module').then( m => m.InsuffisantPageModule)
  },
  {
    path: 'modification-plats-insuffisant',
    loadChildren: () => import('./modification-plats-insuffisant/modification-plats-insuffisant.module').then( m => m.ModificationPlatsInsuffisantPageModule)
  },
  {
    path: 'preparation',
    loadChildren: () => import('./preparation/preparation.module').then( m => m.PreparationPageModule)
  },
  {
    path: 'moncompte',
    loadChildren: () => import('./moncompte/moncompte.module').then( m => m.MoncomptePageModule)
  },
  {
    path: 'mot-de-passe-oublier',
    loadChildren: () => import('./mot-de-passe-oublier/mot-de-passe-oublier.module').then( m => m.MotDePasseOublierPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
