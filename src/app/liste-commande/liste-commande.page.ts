import { Router, NavigationExtras } from '@angular/router';
import { RequetteService } from './../service/requette.service';
import { AuthentificationService } from './../service/authentification.service';
import { DiversService } from './../service/divers.service';
import { Component, OnInit } from '@angular/core';
import { AlertController,ModalController } from '@ionic/angular';

@Component({
  selector: 'app-liste-commande',
  templateUrl: './liste-commande.page.html',
  styleUrls: ['./liste-commande.page.scss'],
})
export class ListeCommandePage implements OnInit {
  commande:any[]=[];
  commandeTemporaire:any[]=[];
  segmentModel = 'tous';
  constructor(
    private router:Router,
    private diversService:DiversService,
    private modalController:ModalController,
    private requetteService:RequetteService,
    private alertController:AlertController,
    private authentificationService:AuthentificationService
  ) { }
  ngOnInit() {
    this.initialisationDonnee();
  }
  valider(numeroCommande){
    this.requetteService.getRequetteGet('updateFermetureCommandes?numeroCommande='+numeroCommande).subscribe((response:any)=>{
      this.diversService.getToastMessage(''+response.message);
      this.getCommande();
      this.filtreCommande(this.segmentModel);
    },erreur=>{
      this.diversService.getAlertControllerAffichage('Message d\'erreur','Il y a une erreur','OK');
      console.log(erreur);
    });
  }
  tabClick(valeur){
    this.segmentModel=valeur;
    this.filtreCommande(this.segmentModel);
  }
  changeTextData(valeur){
    if(valeur==='11'){
      return 'v'
    }else{
      return 'n';
    }
  }
  filtreCommande(model){
    this.commandeTemporaire=this.commande;
    if(model==='valider'){
      this.commandeTemporaire = this.commandeTemporaire.filter((item) => {return (this.changeTextData(''+item.fermetureCommande).toLowerCase().indexOf('v') > -1 );});
    }
    if(model==='nonvalider'){
      this.commandeTemporaire = this.commandeTemporaire.filter((item) => {return (this.changeTextData(''+item.fermetureCommande).toLowerCase().indexOf('n') > -1 );});
    }
    if(model==='tous'){
      this.commandeTemporaire=this.commande;
    }
  }
  initialisationDonnee(){
    this.authentificationService.getVerificationSessionTokenSiExiste();
    this.getCommande();
  }
  getCommande(){
    this.authentificationService.getToken().then((response:string)=>{
      this.requetteService.getRequetteGet('commandesCompletMesCommandes?numeroToken='+response).subscribe((responseSuccess:any[])=>{
        this.commande=responseSuccess;
        this.commandeTemporaire=this.commande;
      },erreur=>{
        this.diversService.getAlertControllerAffichage('Message d\'erreur','Il y a une erreur','OK');
        console.log(erreur);
      });
    }).catch((erreur)=>{
      this.diversService.getAlertControllerAffichage('Message d\'erreur','Il y a une erreur','OK');
      console.log(erreur);
    });
  }
  supprimerCommande(commande:any){
    if(commande.fermetureCommande===1){
      var data={ idCommande : commande.idCommande };
    this.requetteService.getRequettePost('insertionAnnulationCommande',data).subscribe((response:any)=>{    
      this.diversService.getToastMessage(''+response.message);
      this.getCommande();
      this.filtreCommande(this.segmentModel);
    },erreur=>{
      this.diversService.getAlertControllerAffichage('Message d\'erreur','Il y a une erreur','OK');
      console.log('erreur :',erreur);
    });
    }else{
      this.diversService.getAlertControllerAffichage('Message','Vous n\'avez pas le droit','OK');
    }
    /**/
  }
  async voirDetail(commande){
    if(commande!=null){
      let navigationExtrs:NavigationExtras={
        state:{
          commande:commande
        }
      }
      this.router.navigate(['detail-commande'],navigationExtrs);
    }
  }
}
