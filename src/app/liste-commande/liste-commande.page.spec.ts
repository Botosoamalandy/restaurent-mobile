import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListeCommandePage } from './liste-commande.page';

describe('ListeCommandePage', () => {
  let component: ListeCommandePage;
  let fixture: ComponentFixture<ListeCommandePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeCommandePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListeCommandePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
