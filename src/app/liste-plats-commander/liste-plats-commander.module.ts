import { DetailCommandePage } from './../detail-commande/detail-commande.page';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListePlatsCommanderPageRoutingModule } from './liste-plats-commander-routing.module';

import { ListePlatsCommanderPage } from './liste-plats-commander.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListePlatsCommanderPageRoutingModule
  ],
  declarations: [ListePlatsCommanderPage]
})
export class ListePlatsCommanderPageModule {}
