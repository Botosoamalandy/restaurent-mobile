import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListePlatsCommanderPage } from './liste-plats-commander.page';

describe('ListePlatsCommanderPage', () => {
  let component: ListePlatsCommanderPage;
  let fixture: ComponentFixture<ListePlatsCommanderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListePlatsCommanderPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListePlatsCommanderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
