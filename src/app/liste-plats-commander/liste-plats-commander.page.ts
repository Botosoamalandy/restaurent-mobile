import { Router } from '@angular/router';
import { StorageService } from './../service/storage.service';
import { DiversService } from './../service/divers.service';
import { RequetteService } from './../service/requette.service';
import { ModalController,AlertController } from '@ionic/angular';
import { Component, OnInit, Input } from '@angular/core';

export interface CommandeParTable{
  idCommande:string,
	idUtilisateur:string,
	nomUtilisateur:string,
	motDePasse:string,
	etatUtilisateur:number,
	dateAjout:string,
	idTablesRestaurant:string,
	numeroTables:string,
	typeDeTables:string,
	numeroCommande:string,
	numeroOriginale:number,
	fermetureCommande:number,
	etatCommande:number,
	dateCommande:string,
	idCommandeTable:string,
	idProduit:string,
	idCategorie:string,
	categorie:string,
	nomProduit:string,
	prixUnitaire:number,
	descriptions:string,
	nomImage:string,
	tva:number,
  remise:number,
  quantite:number,
	nombreDeProduit:number,
	prixTotal:number,
	etatCommandeTable:number;
}

@Component({
  selector: 'app-liste-plats-commander',
  templateUrl: './liste-plats-commander.page.html',
  styleUrls: ['./liste-plats-commander.page.scss'],
})
export class ListePlatsCommanderPage implements OnInit {
  commandeTemporaire:any={};
  commande:any={
    idCommande : '',
    idUtilisateur : '',
    nomUtilisateur : '',
    motDePasse : '',
    etatUtilisateur : 0,
    dateAjout : '',
    idTablesRestaurant : '',
    numeroTables : '',
    typeDeTables : '',
    numeroCommande : '',
    numeroOriginale : 0,
    fermetureCommande : 0,
    etatCommande : 0,
    dateCommande : ''
  };
  commandeParTables:CommandeParTable[]=[];
  constructor(
    private router:Router,
    private diversService:DiversService,
    private storageService:StorageService,
    private modalController:ModalController,
    private alertController:AlertController,
    private requetteService:RequetteService
  ) {}
  ngOnInit() {
    if(this.router.getCurrentNavigation().extras.state){
      this.commandeTemporaire=this.router.getCurrentNavigation().extras.state.commande;
      this.getCommande(this.commandeTemporaire.idCommande);
      this.getCommandeParTable();
    }
  }
  getCommande(idCommande:string){
    this.requetteService.getRequetteGet('commandesCompletByIdCommandeMobile?idCommande='+idCommande).subscribe((response:any[])=>{
      this.commande=response;
    },erreur=>{
      console.log('erreur',erreur);
      this.diversService.getAlertControllerAffichage('Message d\'erreur','Il y a une erreur','OK');
    });
  }
  getCommandeParTable(){
    this.requetteService.getRequetteGet('commandeParTable?numeroCommande='+this.commandeTemporaire.numeroCommande).subscribe((response:any[])=>{
      console.log('CommandeTable :',response);
      this.commandeParTables=response;var size=this.commandeParTables.length;
      for (let index = 0; index < size; index++) {
        this.storageService.getImageProduit(this.commandeParTables[index].idProduit).then((response:string)=>{
          this.commandeParTables[index].nomImage=response;
        }).catch(erreur=>{
          return '';
        })  
      }
      
    },erreur=>{
      console.log('erreur',erreur);
      this.diversService.getAlertControllerAffichage('Message d\'erreur','Il y a une erreur','OK');
    });
  }
  getImageProduit(idProduit:string){
    return this.storageService.getImageProduit(idProduit).then((response:string)=>{
      return response;
    }).catch(erreur=>{
      return '';
    })
  }
  createInputs() {
    const theNewInputs = [];let size=20;
    var check=true;
    for (let i = 1; i <= size; i++) {
      theNewInputs.push(
        {
          name: ''+i,
          type: 'radio',
          label: ''+i,
          value: ''+i,
          checked: check
        }
      );
      check=false;
    }
    return theNewInputs;
  }
  async chargerQuantite(idCommandeTables) {
    const alerts = await this.alertController.create({
      header: 'Choisez votre quantite',
      inputs:this.createInputs(),
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: ['secondary'],
          handler: (response) => {
          }
        }, {
          text: 'Definir',
          handler: (response) => {
            this.requetteService.getRequetteGet('updateCommandeTable?idCommandeTable='+idCommandeTables+'&quantite='+response).subscribe((response:any)=>{
              this.diversService.getAlertControllerAffichage('Message',''+response.message,'Valider');
              this.getCommandeParTable();
            },erreur=>{
              this.diversService.getAlertControllerAffichage('Message','Il y a une erreur','OK');
              console.log('ereur',erreur);
            });
          }
        }
      ]
    });
    await alerts.present();
  }
  deletePlatsCommande(commandeParTableObject){
    if(commandeParTableObject.fermetureCommande==1){
      this.requetteService.getRequetteGet('supprimerCommandeTable?idCommandeTable='+commandeParTableObject.idCommandeTable).subscribe((response:any)=>{
        this.diversService.getAlertControllerAffichage('Message',''+response.message,'Valider');
        this.getCommandeParTable();
      },erreur=>{
        this.diversService.getAlertControllerAffichage('Message','Il y a une erreur','OK');
        console.log('ereur',erreur);
      });
    }else{
      this.diversService.getAlertControllerAffichage('Message','Suppression echoué car La commande est déjà validée','OK');
    }
  }
}
