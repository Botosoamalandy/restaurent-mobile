import { interval } from 'rxjs';
import { DiversService } from './../service/divers.service';
import { RequetteService } from './../service/requette.service';
import { AuthentificationService } from './../service/authentification.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-preparation',
  templateUrl: './preparation.page.html',
  styleUrls: ['./preparation.page.scss'],
})
export class PreparationPage implements OnInit {
  preparations:any[]=[];
  constructor(
    private diversService:DiversService,
    private requetteService:RequetteService,
    private authentificationService:AuthentificationService
  ) { }

  getPreparation(){
    this.authentificationService.getToken().then((response:string)=>{
      this.requetteService.getRequetteGet('listePreparationPretByIdUtilisateur?numeroToken='+response).subscribe((succesResponse:any[])=>{
        this.preparations=succesResponse;
        console.log(succesResponse);
      },error=>{
        this.diversService.getAlertControllerAffichage('Message d\'erreur','Il y a une erreur lors de la chargement du commande','OK');
      });
    });
  }
  intitialisationFonction(){
    this.authentificationService.getVerificationSessionTokenSiExiste();
    this.getPreparation();
  }
  ngOnInit() {
    this.intitialisationFonction();
    interval(5000).subscribe(x=>{
      this.getPreparation();
    },error=>{
      console.log('Erreur')
    });
  }


}
