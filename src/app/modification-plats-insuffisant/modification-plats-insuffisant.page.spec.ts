import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ModificationPlatsInsuffisantPage } from './modification-plats-insuffisant.page';

describe('ModificationPlatsInsuffisantPage', () => {
  let component: ModificationPlatsInsuffisantPage;
  let fixture: ComponentFixture<ModificationPlatsInsuffisantPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModificationPlatsInsuffisantPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ModificationPlatsInsuffisantPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
