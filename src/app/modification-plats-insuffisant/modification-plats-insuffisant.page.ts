import { AlertController } from '@ionic/angular';
import { StorageService, Produit } from './../service/storage.service';
import { DiversService } from './../service/divers.service';
import { RequetteService } from './../service/requette.service';
import { CommandeParTable } from './../detail-commande/detail-commande.page';
import { Router } from '@angular/router';
import { AuthentificationService } from './../service/authentification.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-modification-plats-insuffisant',
  templateUrl: './modification-plats-insuffisant.page.html',
  styleUrls: ['./modification-plats-insuffisant.page.scss'],
})
export class ModificationPlatsInsuffisantPage implements OnInit {
  commandeParTables:CommandeParTable[]=[];
  testSiValeurVide:boolean=false;
  idSelectetProduit=[];
  insuffisant:any={
    idInsuffisant : '',
	  idUtilisateur : '',
	  idCommande : '',
	  numeroCommande : '',
	  nombreDeProduit:0,
	  etatInsuffisant:0,
	  dateInsuffisant: ''
  };
  
  constructor(
    private router:Router,
    private diversService:DiversService,
    private storageService:StorageService,
    private requetteService:RequetteService,
    private alertController:AlertController,
    private authentificationService:AuthentificationService
  ) { }
  changeEtatCommandeEnNonValider(){
    if(this.insuffisant.idInsuffisant!='' && this.insuffisant.idCommande!=''){
      this.requetteService.getRequetteGet('supprimerInsuffisantEtModifierCommande?idInsuffisant='+this.insuffisant.idInsuffisant+'&idCommande='+this.insuffisant.idCommande).subscribe((response:any)=>{
        this.diversService.getAlertControllerAffichage('Message',''+response.message,'Valider');
      },erreur=>{
        this.diversService.getAlertControllerAffichage('Message','Il y a une erreur','OK');
        console.log('ereur',erreur);
      });
    }
  }
  setImageToImageBase64(commandeParTable:any[]){
    var size=commandeParTable.length;
      for (let index = 0; index < size; index++) {
        this.storageService.getImageProduit(commandeParTable[index].idProduit).then((response:string)=>{
          commandeParTable[index].nomImage=response;
        }).catch(erreur=>{
          return '';
        })  
      }
      return commandeParTable;
  }
  getCommandeParTable(){
    this.requetteService.getRequetteGet('commandeParTableByIdCommande?idCommande='+this.insuffisant.idCommande).subscribe((response:any[])=>{
      this.commandeParTables=this.setImageToImageBase64(response);
    },erreur=>{
      this.diversService.getAlertControllerAffichage('Message','Il y a une erreur','OK');
      console.log('ereur',erreur);
    });
  }
  initialisationFonction(){
    if(this.router.getCurrentNavigation().extras.state){
      this.insuffisant=this.router.getCurrentNavigation().extras.state.insuffisant;
      this.getCommandeParTable();
      console.log(this.commandeParTables);
      this.testSiValeurVide=true;
    }else{
      this.testSiValeurVide=false;
    }
  }

  ngOnInit() {
    this.initialisationFonction();
  }
  modificationTotal(index,prixUnitaire,quantite){
    var idTotal='total'+index;
    document.getElementById(idTotal).innerHTML=''+(prixUnitaire*quantite);
  }
  modificationCommandeParTable(commandeParTable:CommandeParTable,index){
    var idSelection='selection'+index;
    var quantiteText=(<HTMLInputElement>document.getElementById(idSelection)).value;
    try {
      var quantite=parseInt(quantiteText);
      if(commandeParTable!=null && quantite>0){
        this.requetteService.getRequetteGet('updateCommandeTable?idCommandeTable='+commandeParTable.idCommandeTable+'&quantite='+quantite).subscribe((response:any)=>{
          this.diversService.getAlertControllerAffichage('Message',''+response.message,'Valider');
          this.getCommandeParTable();
        },erreur=>{
          this.diversService.getAlertControllerAffichage('Message','Il y a une erreur','OK');
          console.log('ereur',erreur);
        });
      }
    } catch (error) {
      
    }
  }
  supprimerCommandeParPlats(commandeParTable:CommandeParTable){
    this.requetteService.getRequetteGet('supprimerCommandeTable?idCommandeTable='+commandeParTable.idCommandeTable).subscribe((response:any)=>{
      this.diversService.getAlertControllerAffichage('Message',''+response.message,'Valider');
      this.getCommandeParTable();
    },erreur=>{
      this.diversService.getAlertControllerAffichage('Message','Il y a une erreur','OK');
      console.log('ereur',erreur);
    });
  }
  createInputs(data:Produit[]) {
    const theNewInputs = [];let size= data.length;
    var check=true;
    for (let i = 0; i < size; i++) {
      theNewInputs.push(
        {
          name: ''+data[i].nomProduit,
          type: 'radio',
          label: ''+data[i].nomProduit,
          value: data[i],
          checked: check
        }
      );
      check=false;
    }
    return theNewInputs;
  }
  insertionPlats(produit:Produit){
    if(produit.idProduit!='' && produit.prixUnitaire>0 && this.insuffisant.numeroCommande!=''){
      var data={
        idProduit : produit.idProduit,
        nombres : 1,
        prixUnitaires : produit.prixUnitaire,
        commande :this.insuffisant.numeroCommande
      }
      this.requetteService.getRequettePost('insertionCommandePlats',data).subscribe((response:any)=>{
        this.diversService.getAlertControllerAffichage('Message',''+response.message,'Valider');
        this.getCommandeParTable();
      },erreur=>{
        this.diversService.getAlertControllerAffichage('Message','Il y a une erreur','OK');
        console.log('ereur',erreur);
      });
    }
  }
  verificationDedoublement(produit:Produit,commandeParTable:CommandeParTable[]){
    var size=commandeParTable.length;var test=false;
    if(size>0 && produit!=null && produit.nomProduit!=''){
      for (let index = 0; index < size; index++) {
        if(commandeParTable[index].nomProduit==produit.nomProduit){
          return true;
        }
      }
    }
    return false;
  }
  async alertControllerProduit(data){
    const alerts = await this.alertController.create({
      header: 'Ajouter une nouvelle plats',
      inputs:data,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: ['secondary'],
          handler: (response) => {
          }
        }, {
          text: 'Ajouter',
          handler: (response) => {
            var dedoublement=this.verificationDedoublement(response,this.commandeParTables);
            if(!dedoublement){
              this.insertionPlats(response);
            }else{
              this.diversService.getAlertControllerAffichage('Message','Cet plats existe déjà dans cet commande','OK');
            }
          }
        }
      ]
    });
    await alerts.present();
  }
  ajouterPlats(){
    this.storageService.getProduit().then((response:Produit[])=>{
      this.alertControllerProduit(this.createInputs(response));
    }).catch(erreur=>{
      console.log(erreur);
    });
    
  }
}
