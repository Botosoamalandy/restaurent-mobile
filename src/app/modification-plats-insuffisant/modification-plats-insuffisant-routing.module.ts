import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModificationPlatsInsuffisantPage } from './modification-plats-insuffisant.page';

const routes: Routes = [
  {
    path: '',
    component: ModificationPlatsInsuffisantPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModificationPlatsInsuffisantPageRoutingModule {}
