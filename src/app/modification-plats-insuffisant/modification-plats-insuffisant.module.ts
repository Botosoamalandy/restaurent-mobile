import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModificationPlatsInsuffisantPageRoutingModule } from './modification-plats-insuffisant-routing.module';

import { ModificationPlatsInsuffisantPage } from './modification-plats-insuffisant.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModificationPlatsInsuffisantPageRoutingModule
  ],
  declarations: [ModificationPlatsInsuffisantPage]
})
export class ModificationPlatsInsuffisantPageModule {}
