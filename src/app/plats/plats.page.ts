import { NavigationExtras, Router } from '@angular/router';
import { DataService } from './../service/data.service';
import { AlertController, ActionSheetController, ModalController } from '@ionic/angular';
import { RequetteService } from './../service/requette.service';
import { DiversService } from './../service/divers.service';
import { StorageService, Produit } from './../service/storage.service';
import { AuthentificationService } from './../service/authentification.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-plats',
  templateUrl: './plats.page.html',
  styleUrls: ['./plats.page.scss'],
})
export class PlatsPage implements OnInit {
  items:Produit[];
  produits:any[];
  commande:any[];
  categorie:any[];
  recherche:string;
  total:number=0;
  numeroCommande:string='';
  nombre=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
  constructor(
    private router:Router,
    private dataService:DataService,
    private diversService:DiversService,
    private storageService:StorageService,
    private alertController:AlertController,
    private requetteService:RequetteService,
    private modalController:ModalController,
    private actionSheetController:ActionSheetController,
    private authentificationService:AuthentificationService
    ){}
  
  ngOnInit() {
    this.initialisationDonnee();
  }
  // intialisation des fonctions
  initialisationDonnee(){
    this.authentificationService.getVerificationSessionTokenSiExiste();
    this.authentificationService.getToken().then((response:string)=>{
      this.getMyCommande();
      this.getProduit();
      this.getNumeroCommande();
      this.getCategorie();
    }).catch(erreur=>{
      this.authentificationService.redirectionLogin();
    })
  }
  getNumeroCommande(){
    this.dataService.getNumeroCommandeInStorage().then((response:string)=>{
      this.numeroCommande=response;
      console.log(response);
    })
  }
  getCategorie(){
    this.requetteService.getRequetteGet('categorie').subscribe((succesResponse:any[])=>{
      this.categorie=succesResponse;
    },error=>{
      this.diversService.getAlertControllerAffichage('Message d\'erreur','Il y a une erreur lors de la chargement du commande','OK');
    });
  }
  getProduit(){
    this.storageService.getProduit().then((response:Produit[])=>{
      this.produits=response;
      this.items=this.produits;
    }).catch(erreur=>{
      console.log(erreur);
    });
  }
  getMyCommande(){
    this.authentificationService.getToken().then((token:string)=>{
      this.requetteService.getRequetteGet('mesCommandeActuel?numeroToken='+token).subscribe((succesResponse:any[])=>{
        this.commande=succesResponse;
      })  
    }).catch(erreur=>{
        this.diversService.getAlertControllerAffichage('Message d\'erreur','Il y a une erreur lors de la chargement du commande','OK');
        console.log('erreur :',erreur);
    });
  }
  createInputs() {
    const theNewInputs = [];let size= this.commande.length;
    var check=true;
    for (let i = 0; i < size; i++) {
      console.log(this.commande[i]);
      theNewInputs.push(
        {
          name: ''+this.commande[i].numeroCommande,
          type: 'radio',
          label: ''+this.commande[i].numeroCommande,
          value: ''+this.commande[i].numeroCommande,
          checked: check
        }
      );
      check=false;
    }
    return theNewInputs;
  }
  async chargerCommande() {
    const alerts = await this.alertController.create({
      header: 'Utiliser une commande',
      inputs:this.createInputs(),
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: ['secondary'],
          handler: (response) => {
          }
        }, {
          text: 'Definir',
          handler: (response) => {
            this.setNumeroCommande(response);
          }
        }
      ]
    });
    await alerts.present();
  }
  setNumeroCommande(numero){
    if(numero!='' && numero!=null){
      this.numeroCommande=numero;
      this.dataService.setNumeroCommandeInStorage(this.numeroCommande);
    }else{
      this.numeroCommande='';
    }
  }
  createDataCategorie():any[]{
    var size=this.categorie.length;var data:any[]=[size+2];
    for (let index = 0; index < size; index++) {
      data[index]={ 
        text : ''+this.categorie[index].categorie,
        icon : 'search-circle',
        handler: () => {
          this.items=this.produits;
          this.items = this.items.filter((item) => {return (item.idCategorie.toLowerCase().indexOf((this.categorie[index].categorie).toLowerCase()) > -1);});
        }
      };
    }
    data[size]={ text : 'Tous', icon : 'search-circle',handler: () => {
      this.items=this.produits;
    }};
    data[size+1]={ text : 'Cancel', icon : 'close',role : 'cancel',handler: () => {
      console.log('Cancel clicked');
    }};
    return data;
  }
  async changeCategorie() {
    var rechercher='';
    const actionSheet = await this.actionSheetController.create({
      header: 'Catégorie',
      buttons: this.createDataCategorie()
    });
    await actionSheet.present();
  }
  initializeItems(){
    this.items=this.produits;
  }
  getRechercheProduit(ev: any) {
    this.initializeItems();
    const val = ev.target.value;
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (
          item.nomProduit.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
          item.idCategorie.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
          (''+item.prixUnitaire).toLowerCase().indexOf(val.toLowerCase()) > -1
        );
      })
    }
  }
  getFiltreProduitByCategorie(categorie:string){
     this.items = this.items.filter((item) => {return (item.nomProduit.toLowerCase().indexOf(categorie.toLowerCase()) > -1);});
  }
  modificationTotal(index,quantite,prixUnitaire){//nombres,prixUnitairePlats
    var idTotal='total'+index;
    document.getElementById(idTotal).innerHTML=''+(prixUnitaire*quantite);
  }
  ajoutCommande(idProduit,prixUnitaire,index){
    var idSelection='selection'+index;
    var quantiteText=(<HTMLInputElement>document.getElementById(idSelection)).value;
    try {
      var quantites=parseInt(quantiteText);
      var data={ idProduit : idProduit, nombres : quantites, prixUnitaires : prixUnitaire, commande : this.numeroCommande};
      this.requetteService.getRequettePost('insertionCommandePlats',data).subscribe((response:any)=>{
        this.diversService.getAlertControllerAffichage('Message',''+response.message,'Valider');
      },erreur=>{
        this.diversService.getAlertControllerAffichage('Message d\'erreur','Il y a une erreur','OK');
      });
    } catch (error) {
      this.diversService.getAlertControllerAffichage('Message d\'erreur','Il y a une erreur','OK');
      console.log('erreur :',error);
    }
  }
   getCommandeByNumeroCommande():any{
    let size=this.commande.length;
    for (let index = 0; index < size; index++) {
      if(this.commande[index].numeroCommande==this.numeroCommande){
        return this.commande[index];
      }
    }
    return null;
  }
  async voirDetail(){
    var temporaireCommande=this.getCommandeByNumeroCommande();
    if(temporaireCommande!=null){
      let navigationExtrs:NavigationExtras={
        state:{
          commande:temporaireCommande
        }
      }
      this.router.navigate(['liste-plats-commander'],navigationExtrs);
    }else{
      this.diversService.getAlertControllerAffichage('Message d\'erreur','Il y a une erreur lors de la chargement du commande','OK');
    }
  }

}
