import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NouvelCommandePageRoutingModule } from './nouvel-commande-routing.module';

import { NouvelCommandePage } from './nouvel-commande.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NouvelCommandePageRoutingModule
  ],
  declarations: [NouvelCommandePage]
})
export class NouvelCommandePageModule {}
