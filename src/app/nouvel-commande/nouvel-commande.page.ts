import { DiversService } from './../service/divers.service';
import { AuthentificationService } from './../service/authentification.service';
import { RequetteService } from './../service/requette.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nouvel-commande',
  templateUrl: './nouvel-commande.page.html',
  styleUrls: ['./nouvel-commande.page.scss'],
})
export class NouvelCommandePage implements OnInit {
  tablesRestaurants:any[]=[];
  mesCommandes:any[]=[]
  table:string;
  constructor(
    private diversService:DiversService,
    private requetteService:RequetteService,
    private authentificationService:AuthentificationService
  ) { }

  ngOnInit() {
    this.initalisationDonnee();
  }
  //initialisation des données
  initalisationDonnee(){
    this.authentificationService.getVerificationSessionTokenSiExiste();
    this.getTablesRestaurant();
    this.getMesCommandes();
  }
  //fonction
  getTablesRestaurant(){
    this.requetteService.getRequetteGet('tablesRestaurant').subscribe((response:any[])=>{
     this.tablesRestaurants=response; 
     console.log(response);
    },error=>{
      console.log('Erreur :',error);
    })
  }
  getMesCommandes(){
    this.authentificationService.getToken().then((token:string)=>{
      this.requetteService.getRequetteGet('mesCommandeByIdUtilisateur?numeroToken='+token).subscribe((succesResponse:any[])=>{
        this.mesCommandes=succesResponse;
      })  
    }).catch(erreur=>{
        this.diversService.getAlertControllerAffichage('Message d\'erreur','Il y a une erreur lors de la chargement du commande','OK');
        console.log('erreur :',erreur);
    })
  }
  //ngClick
  ajoutCommande(){
    this.authentificationService.getToken().then((response:string)=>{
      if(this.table!=null && this.table!='' && response!=null && response!=''){
        var data={ numeroTable : this.table , tokenUtilisateurs : response};
        this.requetteService.getRequettePost('insertionCommande',data).subscribe((success:any)=>{
          this.diversService.getAlertControllerAffichage('Message',''+success.message,'Valider');
          this.getMesCommandes();
        },erreur=>{
          this.diversService.getAlertControllerAffichage('Message d\'erreur','Il y a une erreur','OK');
          console.log('erreur :',erreur);
        });
      }else{
        this.diversService.getAlertControllerAffichage('Message d\'erreur','Verifiez votre selection car il y a une erreur','OK');
      }
    }).catch(erreur=>{
        this.diversService.getAlertControllerAffichage('Message d\'erreur','Il y a une erreur','OK');
        console.log('erreur :',erreur);
    })
  }
  supprimerCommande(idCommande,fermetureCommande){
    if(fermetureCommande==1){
      var data={ idCommande : idCommande };
      this.requetteService.getRequettePost('insertionAnnulationCommande',data).subscribe((response:any)=>{    
        this.diversService.getAlertControllerAffichage('Message',''+response.message,'Valider');
        this.getMesCommandes();
      },erreur=>{
        this.diversService.getAlertControllerAffichage('Message d\'erreur','Il y a une erreur','OK');
        console.log('erreur :',erreur);
      });
    }else{
      this.diversService.getAlertControllerAffichage('Message','Vous n\'avez pas le droit de supprimé cet commande','OK');
    }
    
  }
}
