import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NouvelCommandePage } from './nouvel-commande.page';

describe('NouvelCommandePage', () => {
  let component: NouvelCommandePage;
  let fixture: ComponentFixture<NouvelCommandePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NouvelCommandePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NouvelCommandePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
