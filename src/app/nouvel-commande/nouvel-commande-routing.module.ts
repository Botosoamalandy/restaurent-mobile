import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NouvelCommandePage } from './nouvel-commande.page';

const routes: Routes = [
  {
    path: '',
    component: NouvelCommandePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NouvelCommandePageRoutingModule {}
