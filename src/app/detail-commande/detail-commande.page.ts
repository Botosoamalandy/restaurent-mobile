import { Router } from '@angular/router';
import { StorageService, Produit } from './../service/storage.service';
import { RequetteService } from './../service/requette.service';
import { ModalController, AlertController } from '@ionic/angular';
import { DiversService } from './../service/divers.service';
import { Component, OnInit, Input } from '@angular/core';

export interface CommandeParTable{
  idCommande:string,
	idUtilisateur:string,
	nomUtilisateur:string,
	motDePasse:string,
	etatUtilisateur:number,
	dateAjout:string,
	idTablesRestaurant:string,
	numeroTables:string,
	typeDeTables:string,
	numeroCommande:string,
	numeroOriginale:number,
	fermetureCommande:number,
	etatCommande:number,
	dateCommande:string,
	idCommandeTable:string,
	idProduit:string,
	idCategorie:string,
	categorie:string,
	nomProduit:string,
	prixUnitaire:number,
	descriptions:string,
	nomImage:string,
	tva:number,
  remise:number,
  quantite:number,
	nombreDeProduit:number,
	prixTotal:number,
	etatCommandeTable:number;
}

@Component({
  selector: 'app-detail-commande',
  templateUrl: './detail-commande.page.html',
  styleUrls: ['./detail-commande.page.scss'],
})

export class DetailCommandePage implements OnInit {

  commande: any={
    idCommande : '',
    idUtilisateur : '',
    nomUtilisateur : '',
    motDePasse : '',
    etatUtilisateur : 0,
    dateAjout : '',
    idTablesRestaurant : '',
    numeroTables : '',
    typeDeTables : '',
    numeroCommande : '',
    numeroOriginale : 0,
    fermetureCommande : 0,
    etatCommande : 0,
    dateCommande : ''
  };
  commandeParTables:CommandeParTable[]=[];
  constructor(
    private router:Router,
    private diversService:DiversService,
    private modalController:ModalController,
    private requetteService:RequetteService,
    private storageService:StorageService,
    private alertController:AlertController
  ) {}
  ngOnInit() {
    if(this.router.getCurrentNavigation().extras.state){
      this.commande=this.router.getCurrentNavigation().extras.state.commande;
      this.getCommandeParTable();
    }
  }
  getCommandeParTable(){
    this.requetteService.getRequetteGet('commandeParTable?numeroCommande='+this.commande.numeroCommande).subscribe((response:any[])=>{
      this.commandeParTables=response;var size=this.commandeParTables.length;
      for (let index = 0; index < size; index++) {
        this.storageService.getImageProduit(this.commandeParTables[index].idProduit).then((response:string)=>{
          this.commandeParTables[index].nomImage=response;
        }).catch(erreur=>{
          return '';
        })  
      }
      console.log(response);
    },erreur=>{
      console.log('erreur',erreur);
      this.diversService.getAlertControllerAffichage('Message d\'erreur','Il y a une erreur','OK');
    });
  }
  getImageProduit(idProduit:string){
    return this.storageService.getImageProduit(idProduit).then((response:string)=>{
      return response;
    }).catch(erreur=>{
      return '';
    })
  }
  modificationTotal(index,prixUnitaire,quantite){
    var idTotal='total'+index;
    document.getElementById(idTotal).innerHTML=''+(prixUnitaire*quantite);
  }
  modificationCommandeParTable(commandeParTable:CommandeParTable,index){
    var idSelection='selection'+index;
    var quantiteText=(<HTMLInputElement>document.getElementById(idSelection)).value;
    try {
      var quantite=parseInt(quantiteText);
      if(commandeParTable!=null && quantite>0){
        this.requetteService.getRequetteGet('updateCommandeTable?idCommandeTable='+commandeParTable.idCommandeTable+'&quantite='+quantite).subscribe((response:any)=>{
          this.diversService.getAlertControllerAffichage('Message',''+response.message,'Valider');
          this.getCommandeParTable();
        },erreur=>{
          this.diversService.getAlertControllerAffichage('Message','Il y a une erreur','OK');
          console.log('ereur',erreur);
        });
      }
    } catch (error) {
      
    }
  }
  supprimerCommandeParPlats(commandeParTable:CommandeParTable){
    this.requetteService.getRequetteGet('supprimerCommandeTable?idCommandeTable='+commandeParTable.idCommandeTable).subscribe((response:any)=>{
      this.diversService.getAlertControllerAffichage('Message',''+response.message,'Valider');
      this.getCommandeParTable();
    },erreur=>{
      this.diversService.getAlertControllerAffichage('Message','Il y a une erreur','OK');
      console.log('ereur',erreur);
    });
  }
  createInputs(data:Produit[]) {
    const theNewInputs = [];let size= data.length;
    var check=true;
    for (let i = 0; i < size; i++) {
      theNewInputs.push(
        {
          name: ''+data[i].nomProduit,
          type: 'radio',
          label: ''+data[i].nomProduit,
          value: data[i],
          checked: check
        }
      );
      check=false;
    }
    return theNewInputs;
  }
  insertionPlats(produit:Produit){
    if(produit.idProduit!='' && produit.prixUnitaire>0 && this.commande.numeroCommande!=''){
      var data={
        idProduit : produit.idProduit,
        nombres : 1,
        prixUnitaires : produit.prixUnitaire,
        commande :this.commande.numeroCommande
      }
      this.requetteService.getRequettePost('insertionCommandePlats',data).subscribe((response:any)=>{
        this.diversService.getAlertControllerAffichage('Message',''+response.message,'Valider');
        this.getCommandeParTable();
      },erreur=>{
        this.diversService.getAlertControllerAffichage('Message','Il y a une erreur','OK');
        console.log('ereur',erreur);
      });
    }
  }
  verificationDedoublement(produit:Produit,commandeParTable:CommandeParTable[]){
    var size=commandeParTable.length;var test=false;
    if(size>0 && produit!=null && produit.nomProduit!=''){
      for (let index = 0; index < size; index++) {
        if(commandeParTable[index].nomProduit==produit.nomProduit){
          return true;
        }
      }
    }
    return false;
  }
  async alertControllerProduit(data){
    const alerts = await this.alertController.create({
      header: 'Ajouter une nouvelle plats',
      inputs:data,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: ['secondary'],
          handler: (response) => {
          }
        }, {
          text: 'Ajouter',
          handler: (response) => {
            var dedoublement=this.verificationDedoublement(response,this.commandeParTables);
            if(!dedoublement){
              this.insertionPlats(response);
            }else{
              this.diversService.getAlertControllerAffichage('Message','Cet plats existe déjà dans cet commande','OK');
            }
          }
        }
      ]
    });
    await alerts.present();
  }
  ajouterPlats(){
    this.storageService.getProduit().then((response:Produit[])=>{
      this.alertControllerProduit(this.createInputs(response));
    }).catch(erreur=>{
      console.log(erreur);
    });
    
  }
}
