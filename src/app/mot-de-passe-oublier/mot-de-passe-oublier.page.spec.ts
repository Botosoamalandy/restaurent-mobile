import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MotDePasseOublierPage } from './mot-de-passe-oublier.page';

describe('MotDePasseOublierPage', () => {
  let component: MotDePasseOublierPage;
  let fixture: ComponentFixture<MotDePasseOublierPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MotDePasseOublierPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MotDePasseOublierPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
