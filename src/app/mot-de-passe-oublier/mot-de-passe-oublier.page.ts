import { Router } from '@angular/router';
import { RequetteService } from './../service/requette.service';
import { DiversService } from './../service/divers.service';
import { MotDePasseDynamiqueService } from './../service/mot-de-passe-dynamique.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mot-de-passe-oublier',
  templateUrl: './mot-de-passe-oublier.page.html',
  styleUrls: ['./mot-de-passe-oublier.page.scss'],
})
export class MotDePasseOublierPage implements OnInit {
  textMotDePasse:string='';nom:string;validationMotDePasse:boolean=false;textConfirmation:string='';motDePasse:string='';
  validationFinale:boolean=false;confirmation:string='';dateAjout:string;
  constructor(
    private router:Router,
    private diversService:DiversService,
    private requetteService:RequetteService,
    private motDePasseDynamiqueService:MotDePasseDynamiqueService
  ) { }

  ngOnInit() {
  }
  setToDynamiqueMotDepasse(ev: any){
    var text=ev.target.value;
    if(this.motDePasseDynamiqueService.getVerificationLenghtWords(text) && 
    this.motDePasseDynamiqueService.getVerificationIfNumberExist(text) && 
    this.motDePasseDynamiqueService.getVerificationIfAlphabSmallWordExist(text) &&
    this.motDePasseDynamiqueService.getVerificationIfAlphabExist(text) &&
    this.motDePasseDynamiqueService.getVerificationIfWordsSpecialExist(text)){
      document.getElementById('textMDP').style.color='green';
      this.textMotDePasse='Mot de passe complet';
      this.validationMotDePasse=true;this.motDePasse=text;this.disabledBoutonOrNon(text);
    }else{
      document.getElementById('textMDP').style.color='red';
      this.validationMotDePasse=false;this.motDePasse='';
      this.textMotDePasse=
        this.motDePasseDynamiqueService.getAssembleLesLettre(this.motDePasseDynamiqueService.getVerificationLenghtWords(text),'8 caractére minimum')+' '+
        this.motDePasseDynamiqueService.getAssembleLesLettre(this.motDePasseDynamiqueService.getVerificationIfNumberExist(text),'un chiffre')+' '+
        this.motDePasseDynamiqueService.getAssembleLesLettre(this.motDePasseDynamiqueService.getVerificationIfAlphabSmallWordExist(text),'une lettre')+' '+
        this.motDePasseDynamiqueService.getAssembleLesLettre(this.motDePasseDynamiqueService.getVerificationIfAlphabExist(text),'un lettre majuscule')+' '+
        this.motDePasseDynamiqueService.getAssembleLesLettre(this.motDePasseDynamiqueService.getVerificationIfWordsSpecialExist(text),'une caractére speciaux');
        this.disabledBoutonOrNon(text);
    }
  }
  confirmationMotDePasse(ev: any){
    var text=ev.target.value;
    if(this.validationMotDePasse==true && this.motDePasse!='' && this.motDePasse==text){
      this.validationFinale=true;
      this.textConfirmation='Mot de passe confirmé';
      document.getElementById('textConfirmation').style.color='green';
      this.disabledBoutonOrNon(text);
    }else{
      this.validationFinale=false;
      this.textConfirmation='Mot de passe non confirmé';
      document.getElementById('textConfirmation').style.color='red';
      this.disabledBoutonOrNon(text);
    }
  }
  disabledBoutonOrNon(text:string){
    if(this.validationFinale==true && this.motDePasse!='' && this.motDePasse==text){
      (<HTMLInputElement>document.getElementById('bouton')).disabled=false;
    }else{
      (<HTMLInputElement>document.getElementById('bouton')).disabled=true;
    }
  }
  modificationMotDePasse(){
    if(this.motDePasse!='' && this.dateAjout!=null && this.dateAjout!='' && this.nom!=null && this.nom!=''){
      var data={
        nom : this.nom,
        dateAjout :this.dateAjout,
        nouveauMotDePasse : this.motDePasse
      }
      this.requetteService.getRequettePost('motDePasseOublier',data).subscribe((response:any)=>{
        this.diversService.getAlertControllerAffichage('Message',''+response.message,'Valider');
        this.router.navigateByUrl('login');
      },erreur=>{
        this.diversService.getAlertControllerAffichage('Message','Il y a une erreur','OK');
        console.log(erreur);  
      });
      console.log('data : ',data)
    }else{
      this.diversService.getAlertControllerAffichage('Message','Il y a une champs vide','OK');
    }
  }
}
