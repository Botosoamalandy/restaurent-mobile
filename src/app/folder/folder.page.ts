import { Produit, StorageService } from './../service/storage.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-folder',
  templateUrl: './folder.page.html',
  styleUrls: ['./folder.page.scss'],
})
export class FolderPage implements OnInit {
  public folder: string;
  //items: Item[]=[];
  //newItem:Item=<Item>{};
  produit:Produit[]=[];

  constructor(private activatedRoute: ActivatedRoute,private storageService:StorageService,private httpClient:HttpClient) { }

  ngOnInit() {
    this.folder = this.activatedRoute.snapshot.paramMap.get('id');
  }
  loadProduit(){
    this.storageService.getProduit().then((successResponse:Produit[])=>{
      this.produit=successResponse;
    });
  }
  saveProduit(){
    var urlTemporaire='http://localhost:8087/produitMobile'
    this.httpClient.get(urlTemporaire,{headers: new HttpHeaders({'Content-Type':'application/json','Accept': 'application/json','Access-Control-Allow-Origin': ''+urlTemporaire })})
    .subscribe((successResponse:any[])=>{
      this.storageService.addProduit(successResponse);
      console.log(successResponse);
    },(errorResponse)=>{
      console.log(errorResponse);
    });
  }
}
