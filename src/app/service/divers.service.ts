import { Injectable } from '@angular/core';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class DiversService {
  constructor(private alertController:AlertController,private loadingController:LoadingController,private toastController:ToastController) { }
  async getAlertControllerAffichage(header:string,messages:string,boutonText:string){
    const alert = await this.alertController.create({
      header: header,
      message: messages,
      backdropDismiss: false,
      buttons:[{
        text: boutonText,
        cssClass: 'secondary'
      }]
    });
    await alert.present();
  }
  async getLoading(){
    let loading= await this.loadingController.create();
    await loading.present();
  }
  async getLoadingWithMessage(message:string){
    let loading= await this.loadingController.create({
      message: ''+message
    });
    await loading.present();
  }
  async getToastMessage(message:string) {
    const toast = await this.toastController.create({
      message:message,
      duration: 2000
    });
    toast.present();
  }
}
