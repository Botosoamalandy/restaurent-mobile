import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {
  private nameItem:string='token';
  constructor(private storage:Storage,private router:Router) { }
  addToken(token:string){
    this.storage.set(this.nameItem,token);
  }
  getToken():Promise<string>{
    return this.storage.get(this.nameItem);
  }
  getVerificationSessionTokenSiExiste(){
    this.getToken().then((response:string)=>{
      if(response==null || response==undefined || response==''){
        this.storage.clear();
        this.router.navigateByUrl('login');
      }
    }).catch((erreur)=>{
      console.log('erreur : ',erreur);
      this.storage.clear();
      this.router.navigateByUrl('login');
    })
  }
  deconnexion(){
    this.storage.remove(this.nameItem);
    this.storage.clear();
    this.router.navigateByUrl('login');
  }
  redirectionLogin(){
    this.storage.clear();
    this.router.navigateByUrl('login');
  }
  
}
