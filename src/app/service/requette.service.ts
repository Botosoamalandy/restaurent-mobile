import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RequetteService {
  private urlRequette:string='http://localhost:8087/';
  constructor(private httpClient:HttpClient) { }
  setUrl(newUrl:string){
    this.urlRequette=newUrl;
  }
  getUrlRequette():string{
    return this.urlRequette;
  }
  getUrlRequetteWithParameter(parameter:string):string{
    return this.urlRequette+''+parameter;
  }
  getRequettePost(parametreUrl:string,data){
    let urlTemporaire=''+this.getUrlRequetteWithParameter(parametreUrl);
    return this.httpClient.post(urlTemporaire,data,{
      headers: new HttpHeaders(
        {
          'Content-Type':'application/json',
          'Accept': 'application/json',
          'Access-Control-Allow-Origin': ''+urlTemporaire
        }
      )
    });
  }
  getRequetteGet(parametreUrl:string){
    let urlTemporaire=this.getUrlRequetteWithParameter(parametreUrl);
    return this.httpClient.get(urlTemporaire,{
      headers: new HttpHeaders(
        {
          'Content-Type':'application/json',
          'Accept': 'application/json',
          'Access-Control-Allow-Origin': ''+urlTemporaire
        }
      )
    });
  }
}
