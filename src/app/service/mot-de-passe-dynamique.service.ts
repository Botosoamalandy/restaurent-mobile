import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MotDePasseDynamiqueService {

  constructor() { }
  
  getVerificationLenghtWords(password:string){//verification si MDP contient 8 caractére
    if(password.length<8){
        return false;
    }
    return true;
  }
  getVerificationIfNumberExist(password:string){// si il y a un chiffre
    if(password.search(/[0123456789]/)==-1){
        return false;
    }
    return true;
  }
  getVerificationIfAlphabSmallWordExist(password:string){
    if(password.search(/[a-z]/)==-1){
        return false;
    }
    return true;
  }
  getVerificationIfAlphabExist(password:string){
    if(password.search(/[A-Z]/)==-1){
        return false;
    }
    return true;
  }
  getVerificationIfWordsSpecialExist(password:string){
    if(password.search(/[\[~!@#|\"$%;^'\\[=&(:?`/<>.,)£ù*§\]]/)==-1){
        return false;
    }
    return true;
  }
  getAssembleLesLettre(valeur:boolean,mot:string){//assemblage des erreurs MDP
    if(valeur==false){
        return mot;
    }
    return '';
  }
}
