import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

export interface Produit{
  idProduit:string,
	idCategorie:string,
	nomProduit:string,
	prixUnitaire:number,
	descriptions:string,
	nomImage:string,
	imageInBase64:string,
	tva:number,
  remise:number,
  quantite:number
}
const itemsName='produit';
@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor(private storage:Storage) { }
  addProduit(produits: Produit[]){
    this.storage.set(itemsName,produits);
  }
  getImageProduit(idProduit:string):Promise<string>{
    return this.storage.get(itemsName).then((response)=>{
      for (let temporaire of response) {
        if(idProduit==temporaire.idProduit){
          return temporaire.imageInBase64;
        }
      }
      return response;
    });
  }
  getProduit():Promise<Produit[]>{
    return this.storage.get(itemsName).then((response)=>{
      return response;
    });
  }
  /*updateItem(item:Item):Promise<any>{
    return this.storage.get(itemsName).then((items:Item[])=>{
      if(!items || items.length===0){
        return null;
      }
      let newItems:Item[]=[];
      for (let i of items) {
        if (i.id ===item.id) {
          newItems.push(item);
        }else{
          newItems.push(i);
        }
      }
      return this.storage.set(itemsName,newItems);
    });
  }*/
  deleteItem(){
    this.storage.clear();
  }
}
