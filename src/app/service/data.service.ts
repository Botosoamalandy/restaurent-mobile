import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  commandeItem='numeroCommande';
  constructor(private storage:Storage) { }
  setNumeroCommandeInStorage(valeur:string){
    this.storage.set(this.commandeItem,valeur);
  }
  getNumeroCommandeInStorage():Promise<string>{
    return this.storage.get(this.commandeItem).then((response)=>{
      if(response!=null && response!=undefined && response!=''){
        return response;
      }else{
        return '';
      }
    }).catch(erreur=>{
      console.log(erreur);
      return '';
    });
  }
}
