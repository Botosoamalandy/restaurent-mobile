import { TestBed } from '@angular/core/testing';

import { MotDePasseDynamiqueService } from './mot-de-passe-dynamique.service';

describe('MotDePasseDynamiqueService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MotDePasseDynamiqueService = TestBed.get(MotDePasseDynamiqueService);
    expect(service).toBeTruthy();
  });
});
